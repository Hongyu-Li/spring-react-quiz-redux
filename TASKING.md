## Story 1: homepage
1. fetch data from backend api via action.
2. create reducer, store.
3. connect component with state and dispatch.
4. click add event

## Story 2: orders detail
1. display orders on the page
2. add delete operation

## Story 3: add products
1. create product actions, reducer.
2. connect component with state and dispatch.
3. click and then send post request.