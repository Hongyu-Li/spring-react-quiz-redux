import React, {Component} from 'react';
import './App.less';
import Home from "./home/components/Home";
import {BrowserRouter, Switch, Route, Link} from "react-router-dom";
import Orders from "./orders/components/Orders";
import CreateProduct from "./createOrder/components/CreateProduct";
import {FaPlus, FaShoppingCart, FaWarehouse} from "react-icons/fa";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <BrowserRouter>
           <header>
                <ul className="nav_bar">
                    <li><FaWarehouse style={{paddingRight:"15px"}}/><Link to='/' style={{ textDecoration: 'none', color:'white'}}>商城</Link></li>
                    <li><FaShoppingCart style={{paddingRight:"15px"}}/><Link to='/orders' style={{ textDecoration: 'none', color:'white'}}>订单</Link></li>
                    <li><FaPlus style={{paddingRight:"15px"}}/><Link to='/products/create' style={{ textDecoration: 'none', color:'white'}}>添加商品</Link></li>
                </ul>
            </header>
          <Switch>
            <Route exact path='/orders' component={Orders}/>
            <Route exact path='/products/create' component={CreateProduct}/>
            <Route exact path='/' component={Home}/>
          </Switch>
        </BrowserRouter>
        <footer>TW Mall &copy;2018 Created by Hongyu</footer>
      </div>
    );
  }
}
export default App;