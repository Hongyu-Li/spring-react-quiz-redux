import {getProducts} from "../../home/actions/getAction";

export const CREATE_PRODUCT = 'CREATE_PRODUCT';

export const createProduct = (name,price,unit,url) => ((dispatch)=>{
    const init = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({name:name,price: price,unit:unit,url:url})
    }

    fetch(
        'http://localhost:8080/api/products', init)
        .then(res => {
          if (res.status === 400){
              alert('商品名称已存在，请输入新的商品名称');
          }else {
              dispatch(getProducts());
              window.location.href = '/';  //redirect to the homepage
          }
        });
});