export const HANDLE_PRICE = "HANDLE_PRICE";

export const handlePrice = (price) => (dispatch)=>{
    dispatch({type: HANDLE_PRICE,
        newProductPrice: price});
};