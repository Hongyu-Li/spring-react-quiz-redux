export const HANDLE_UNIT = "HANDLE_UNIT";

export const handleUnit = (unit) => (dispatch) => {
    dispatch({
        type: HANDLE_UNIT,
        newProductUnit: unit
    })
};