export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_ORDERS = 'GET_ORDERS';

export const getProducts = () => ((dispatch)=>{
    const url = 'http://localhost:8080/api/products';
    fetch(url)
        .then(response => response.json())
        .then(data=>{
            dispatch({type: GET_PRODUCTS,
                products: data})
        })
});


export const getOrders = () => ((dispatch)=>{
    const url = 'http://localhost:8080/api/orders';
    fetch(url)
        .then(response => response.json())
        .then(data=>{
            dispatch({type: GET_ORDERS,
                orders: data})
        })
});