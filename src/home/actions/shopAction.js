import {getOrders} from "./getAction";

export const SHOP_PRODUCT = "SHOP_PRODUCT";

// export const shopProduct = (order) => ((dispatch)=>{
//     dispatch({type: "SHOP_PRODUCT",
//         order: order})
// });

export const shopProduct = (name) => (()=>{
    const init = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({name:name})
    }

    fetch(
        'http://localhost:8080/api/orders/', init)
        .then();
});