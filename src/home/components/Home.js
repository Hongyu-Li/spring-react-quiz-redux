import React, {Component} from 'react';
import {connect} from "react-redux";
import {getProducts} from "../actions/getAction";
import "./home.less";
import ItemInfo from "./ItemInfo";


class Home extends Component {


    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        const data = this.props.products;
        return (
                <div className="home">
                    {data.map(item => <ItemInfo imgURL={item.url} name = {item.name} price={item.price} unit={item.unit}/>)}
                </div>
        );
    }
}

const mapStateToProps = state => ({
    products: state.product.products
});

const mapDispatchToProps = dispatch => ({
    getProducts: () => dispatch(getProducts())
});


export default connect(mapStateToProps,mapDispatchToProps)(Home);