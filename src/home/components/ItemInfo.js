import React, {Component} from 'react';
import "./itemInfo.less";
import {shopProduct} from "../actions/shopAction";
import {connect} from "react-redux";

class ItemInfo extends Component {

    constructor(props, context) {
        super(props, context);
        // this.state = {
        //     quantity: 0
        // };
        this.addToCart = this.addToCart.bind(this);
    }

    // addToCart(){
    //     this.setState((prev)=>{
    //             return {quantity: prev.quantity + 1}
    //     },()=>{
    //         const data = {name: this.props.name, price: this.props.price,
    //             quantity: this.state.quantity, unit: this.props.unit};
    //         this.props.shopProduct(data);
    //     });
    // }

    addToCart(){
        this.props.shopProduct(this.props.name);
    }

    render() {
        return (
            <div className="item">
                <img src={this.props.imgURL} />
                <h4>{this.props.name}</h4>
                <span>单价：{this.props.price}元/{this.props.unit}</span><br/>
                <button className ='add-btn' onClick={this.addToCart}>+</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    orders: state.product.orders
});

const mapDispatchToProps = dispatch => ({
    shopProduct: (order) => dispatch(shopProduct(order))
});


export default connect(mapStateToProps,mapDispatchToProps)(ItemInfo);