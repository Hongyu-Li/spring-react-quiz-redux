import {getOrders} from "../../home/actions/getAction";

export const DELETE_ORDERS = "DELETE_ORDERS";

// export const deleteOrders = (order) => ((dispatch)=>{
//     dispatch({type: "DELETE_ORDERS",
//         order: order})
// });

export const deleteOrders = (name) => ((dispatch)=>{
    const init = {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };

    fetch(
        'http://localhost:8080/api/orders/' + encodeURIComponent(name), init)
        .then(res=>{
            if (res.ok){
                dispatch(getOrders());
            }else{
                res.text().then(message=>alert(message));
            }
        })
});