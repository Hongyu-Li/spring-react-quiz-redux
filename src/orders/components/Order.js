import {connect} from "react-redux";
import {deleteOrders} from "../actions/deleteActions";
import React, {Component} from 'react';
import {getOrders} from "../../home/actions/getAction";

class Order extends Component {


    constructor(props, context) {
        super(props, context);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(){
        // const order = {name: this.props.name, price: this.props.price,
        //     quantity: this.props.quantity, unit: this.props.unit};
        this.props.deleteOrders(this.props.name);
    }

    render() {
        return (
            <tr>
                <td>{this.props.name}</td>
                <td>{this.props.price}</td>
                <td>{this.props.quantity}</td>
                <td>{this.props.unit}</td>
                <td><button onClick={this.handleDelete} className="delete-btn">删除</button></td>
            </tr>
        );
    }
}

const mapStateToProps = state => ({
    orders: state.product.orders
});

const mapDispatchToProps = dispatch => ({
    deleteOrders: (name) => dispatch(deleteOrders(name))
});


export default connect(mapStateToProps,mapDispatchToProps)(Order);