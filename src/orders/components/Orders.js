import React, {Component} from 'react';
import "./orders.less";
import OrdersTable from "./OrdersTable";

class Orders extends Component {
    render() {
        return (
            <div className="orders">
                <OrdersTable />
            </div>
        );
    }
}

export default Orders;