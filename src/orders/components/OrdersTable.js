import React, {Component} from 'react';
import {connect} from "react-redux";
import Order from "./Order";
import "./orderstable.less"
import {getOrders} from "../../home/actions/getAction";

class OrdersTable extends Component {

    componentDidMount() {
        this.props.getOrders();
    }

    render() {
        const orders = this.props.orders || [];
        return (
            <div className="order-details">
                {orders.length==0?"暂无订单，返回商城继续购买":
                    <table>
                        <tbody>
                            <tr>
                                <th>名字</th>
                                <th>单价</th>
                                <th>数量</th>
                                <th>单位</th>
                                <th>操作</th>
                            </tr>
                            {orders.map(item=><Order name={item.name} price={item.price}
                                             quantity={item.quantity} unit={item.unit}/>)}
                        </tbody>
                    </table>
                }

            </div>
        );
    }
}


const mapStateToProps = state => ({
    orders: state.product.orders
});

const mapDispatchToProps = dispatch => ({
    getOrders: () => dispatch(getOrders())
});


export default connect(mapStateToProps,mapDispatchToProps)(OrdersTable);
