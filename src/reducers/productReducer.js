import {GET_ORDERS, GET_PRODUCTS} from "../home/actions/getAction";
import {SHOP_PRODUCT} from "../home/actions/shopAction";
import {DELETE_ORDERS} from "../orders/actions/deleteActions";
import {HANDLE_NAME} from "../createOrder/actions/handleName";
import {HANDLE_PRICE} from "../createOrder/actions/handlePrice";
import {HANDLE_UNIT} from "../createOrder/actions/handleUnit";
import {HANDLE_URL} from "../createOrder/actions/handleUrl";

const INIT_STATE = {products:[],orders:[]};

// export default function productReducer(state=INIT_STATE,action) {
//     if (action.type===GET_PRODUCTS){
//         return {
//             ...state,
//             products: action.products
//         }
//     }else if (action.type===SHOP_PRODUCT){
//         const prevOrders = [...state.orders];
//         if (prevOrders.filter(item=>item.name === action.order.name).length === 0){
//             return {
//                 ...state,
//                 orders: [...state.orders, action.order]
//             }
//         }else{
//             const otherOrders = prevOrders.filter(item=>item.name != action.order.name);
//             return {
//                 ...state,
//                 orders: [...otherOrders, action.order]
//             }
//         }
//     }else if (action.type===DELETE_ORDERS){
//         const prevOrders = [...state.orders];
//         const otherOrders = prevOrders.filter(item=>item.name != action.order.name)
//         return {
//             ...state,
//             orders: otherOrders
//         }
//     }else if (action.type===HANDLE_NAME){
//         return {
//             ...state,
//             newProductName: action.newProductName
//         }
//     }else if (action.type===HANDLE_PRICE){
//         return {
//             ...state,
//             newProductPrice: action.newProductPrice
//         }
//     }else if (action.type===HANDLE_UNIT){
//         return {
//             ...state,
//             newProductUnit: action.newProductUnit
//         }
//     }else if (action.type===HANDLE_URL){
//         return {
//             ...state,
//             newProductUrl: action.newProductUrl
//         }
//     }
//     return state;
// }

export default function productReducer(state=INIT_STATE,action) {
    if (action.type===GET_PRODUCTS){
        return {
            ...state,
            products: action.products
        }
    }else if (action.type===SHOP_PRODUCT || action.type===GET_ORDERS || action.type===DELETE_ORDERS){
        return {
            ...state,
            orders: action.orders
        }
    }else if (action.type===HANDLE_NAME){
        return {
            ...state,
            newProductName: action.newProductName
        }
    }else if (action.type===HANDLE_PRICE){
        return {
            ...state,
            newProductPrice: action.newProductPrice
        }
    }else if (action.type===HANDLE_UNIT){
        return {
            ...state,
            newProductUnit: action.newProductUnit
        }
    }else if (action.type===HANDLE_URL){
        return {
            ...state,
            newProductUrl: action.newProductUrl
        }
    }
    return state;
}